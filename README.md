# Hamming Code

This is an implementation in Rust of the Hamming code as seen in Advanced Networking course, dipensed by Alessandro Palumbo.

## Getting started 

Assuming you have cargo installed ( if not see [here](https://doc.rust-lang.org/book/ch01-01-installation.html) for instruction, Rust is the future so I highly recommand installing cargo).

```
cd Rust
cargo run
```

## Why a Rust folder ?

Other implementation may come, so [stay tuned.](https://i.pinimg.com/originals/1f/9e/54/1f9e543757ead4247c378c685890413d.jpg) 



