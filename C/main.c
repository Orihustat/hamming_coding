#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <limits.h>

bool isLineContainOneOne(char * line, int size){
    bool one = false;
    for(int i=0; i<size; i++){
        if(line[i]==1){
            if(one){
                return false;
            }
            one = true;
        }
    }
    return one;
}

char getCode(char * partialCoded, char ** HMatrix, int lenghtMatrix, int nbCode, int i){
    unsigned int res = 0;
    i++;
    for(; i<lenghtMatrix; i++){
        if(HMatrix[i][nbCode]==1){
            res= res^partialCoded[i];
        }
    }
    return res;
}

char * calcCoded(char * word, char ** HMatrix, int lenght, int p){
    const long n = (long) pow(2.0, (double) p) -1;
    const long k = (long) pow(2.0, (double) p) -1 - p;
    int incWord = k-1;
    int incCode = p-1;
    char * coded = malloc(lenght * sizeof(char));
    if(coded == NULL) 
        goto errCalcCoded;
    for(int i=0; i<lenght; i++){
        coded[i] = -1;
    }
    for(int i=lenght-1; i>=0; i--){
        if(isLineContainOneOne(HMatrix[i], p)){
	    printf("%d\n", incCode);
            coded[i] = getCode(coded, HMatrix, lenght, incCode, i);   
            incCode--;
        }
        else{
            coded[i] = word[incWord];
            incWord--; 
        }
    }

    return coded;
errCalcCoded:
    fprintf(stderr, "Error while allocating memory\n");
    return NULL;
}

char * freeArray(char * array){
    if(array != NULL){
        free(array);
    }
}


void freeHMatrix(char** matrix,  int p) {
    int nbColumn = (double) pow(2.0, (double) p)-1;
    if(matrix != NULL){
        for(int i=0; i<nbColumn; i++){
            if(matrix[i]!=NULL){
                free(matrix[i]);
            }
        }
    free(matrix);
    }
}

char ** generateHMatrix(int p) {
    int nbColumn = (double) pow(2.0, (double) p)-1;
    char ** matrix = malloc(nbColumn * sizeof(char * ));
    if(matrix == NULL) {
        goto errGenerateHMatrix;
    }
    for(int i=0; i<nbColumn; i++){
        int a = i+1;
        matrix[i] = malloc(p * sizeof(char));
        if(matrix[i] == NULL){
            goto errGenerateHMatrix;
        }
        for(int j=0; j<p; j++){
            matrix[i][j] = a%2;
            a = a>>1;
        }
    }
    return matrix;
errGenerateHMatrix : 
    fprintf(stderr, "Error allocating memory\n");
    freeHMatrix(matrix, p);
    return NULL;
}

//Converting the string provided by user to array of 0 and 1
char * raw_to_bin_array(const long p, char* arg) {
    const long k = (long) pow(2.0, (double) p) -1 - p;

    char *array = malloc(sizeof(char) * k);
    if( array == NULL)
	goto err_rtba;

    if(k != strlen(arg))
	goto err_rtba;

    for(int i=0; i<k; i++){
	array[i] = arg[i] - 0x30;
    }

end_rtba:
    return array;
err_rtba:
    if(array != NULL) {
	free(array);
    }
    fprintf(stderr, "Error while parsing the input\n");
    return NULL;
}

char * calc_syndrome(char * coded, char ** matrix, const long p) {
    const long n = (long) pow(2.0, (double) p) -1;

    char * syndrome = malloc(sizeof(char) * p);
    if(syndrome == NULL)
	goto err_decode;
    
    for(int i = 0; i< n; i++){
	syndrome[i] = 0;
	for(int j = 0; j< n; j++) {
	    syndrome[i] = syndrome[i] ^ (coded[j] & matrix[j][i]);
	}
    }

    
    return syndrome;
err_decode:
    if(syndrome != NULL)
	free(syndrome);
    fprintf(stderr, "Error while decoding\n");
    return NULL;
}

int main(char argc, char ** argv) {
    if(argc != 3) {
	printf("Wrong number of argument provided\n");
	printf("Usage :\n hamming <p> <code>\nWith <code> in base 2 \n Exemple :\nhamming 3 1101\n");
	return 0;
    }
    
    const long p = strtol(argv[1], NULL, 10);
    if( p <= 0 || p == LONG_MAX || p == LLONG_MAX ){
	fprintf(stderr, "Error p out of range or not formated correctly\np must be a positive long number in base 10\n");
	return -1;
    }

    char * input = raw_to_bin_array(p, argv[2]);
    if( input == NULL)
	goto end_main;

    char ** matrix = generateHMatrix(p);
    if( matrix == NULL)
	goto end_main;
    int nbColumn = (double) pow(2.0, (double) p)-1;
    for(int i=0; i<nbColumn; i++){
        for(int j=0; j<p; j++){
            printf("%u", matrix[i][j]);
        }
        printf("\n");
    }

    char * coded = calcCoded(input, matrix, nbColumn, p);
    if(coded == NULL) 
	goto end_main;
    for(int i = 0; i<nbColumn; i++){
        printf("%u", coded[i]);
    }
    printf("\n");

    char * syndrome = calc_syndrome(coded, matrix, p);
    if(syndrome == NULL) 
	goto end_main;
    for(int i = 0; i<p; i++){
        printf("%u", syndrome[i]);
    }
    printf("\n");




end_main:
    freeArray(syndrome);
    freeArray(input);
    freeArray(coded);
    freeHMatrix(matrix, p);
    return 0;
}
