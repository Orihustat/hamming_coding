
fn generate_line_i(p : u32,mut i : u32) -> Vec<u8> {
    let mut ret : Vec<u8> = Vec::new();

    for _ in 0..p {
        ret.push((i%2) as u8);
        i = i>>1;
    }

    ret
}


fn generate_h_matrix(p : u32) -> Vec<Vec<u8>> {
    let mut ret : Vec<Vec<u8>> = Vec::new();

    let nb_column = 2u32.pow(p);

    for i in 1..nb_column {
        ret.push(generate_line_i(p, i));
    }
    ret
}

fn calc_ci(h_matrix : &Vec<Vec<u8>>, coded : &Vec<u8>, i: usize) -> u8 {
    let mut ret: u8 = 0;
    for (i_coded, column) in h_matrix[(i+1)..].iter().enumerate() {
        if *column.get(i).unwrap() == 1 {
            ret = ret^coded.get(i_coded+i+1).unwrap();
        }
    }
    ret
}

fn create_code(h_matrix : &Vec<Vec<u8>>, input : &Vec<u8>, p: u32) -> Vec<u8> {
    let mut ret : Vec<u8> = Vec::new();
    let n : u32 = 2u32.pow(p) -1;

    let mut i_input  = input.len();
    let mut i_ci = p;
    
    for _ in 0..n {
        ret.push(0);
    }


    for (i, column) in h_matrix.iter().enumerate().rev() {
        println!("{:?}", column);
        if column.iter().filter(| a | {a==&&1}).count() == 1 {
           *ret.get_mut(i).unwrap() = calc_ci(h_matrix, &ret, (i_ci -1)as usize);
           i_ci -= 1; 
        }
        else {
            *ret.get_mut(i).unwrap() = *input.get(i_input-1).unwrap();
            i_input-= 1;
        }
    }
    ret
}

fn main() {
    let p : u32 = 3;

    
    let k = 2u32.pow(p) -1 - p;

    let h_matrix = generate_h_matrix(p);

   // println!("{:?}", h_matrix);

    let input : Vec<u8> = vec![1, 1, 1, 1];


    println!("{:?}", create_code(&h_matrix, &input, p));

}
